use structs::Area;
use x11::xlib::{PropModeReplace, XA_ATOM, XConnectionNumber, XDeleteProperty, XDestroyWindowEvent, XMapWindow, XMoveResizeWindow};
use x11::xlib::{XA_WINDOW, XChangeProperty, XCreateSimpleWindow, XInternAtom};
use std::{ffi::CString, collections::HashMap, mem::MaybeUninit, mem::zeroed, ptr::null, sync::Mutex};

use lazy_static::lazy_static;
use nix::{libc::{EXIT_SUCCESS, c_char, c_uchar, execvp, fork, perror, setsid}, sys::signal::{SigHandler, Signal, signal}};
use x11::{xlib::{AnyKey, AnyModifier, Atom, BadAccess, BadDrawable, BadMatch, BadWindow, Display, EnterWindowMask, False, FocusChangeMask, GrabModeAsync, MapRequest, MappingKeyboard, PropertyChangeMask, Screen, StructureNotifyMask, SubstructureRedirectMask, True, Window, XCloseDisplay, XDefaultRootWindow, XErrorEvent, XEvent, XFreeModifiermap, XGetModifierMapping, XGetWMName, XGetWindowAttributes, XGrabKey, XKeyEvent, XKeycodeToKeysym, XKeysymToKeycode, XMapRequestEvent, XMappingEvent, XNextEvent, XOpenDisplay, XRefreshKeyboardMapping, XRootWindowOfScreen, XScreenCount, XScreenOfDisplay, XSelectInput, XSetErrorHandler, XSync, XTextProperty, XUngrabKey, XWindowAttributes}};
use x11::keysym::*;
use x11::xlib::{Mod1Mask, ControlMask, Mod2Mask, ShiftMask, Mod3Mask, Mod4Mask, Mod5Mask, LockMask};
use utils::{ccstr, cstr};

pub mod utils;
pub mod config;
pub mod structs;

////// MACROS

macro_rules! map(
{ $($key:expr => $value:expr),+ } => {
    {
        let mut m = ::std::collections::HashMap::new();
        $(
            m.insert($key, $value);
        )+
        m
    }
 };
);

////// ENUMS

#[allow(dead_code)]
enum Cursor { CurNormal, CurResize, CurMove, CurLast }
#[allow(dead_code)] 
enum WMAtom { WMProtocols, WMDelete, WMState, WMTakeFocus, WMLast }
#[allow(dead_code)]
enum NetAtom {  NetSupported, NetWMName, NetWMState, NetWMCheck,
                NetWMFullscreen, NetActiveWindow, NetWMWindowType,
                NetWMWindowTypeDialog, NetClientList, NetLast }

////// CONSTS and STATICS

const VERSION: &'static str = env!("CARGO_PKG_VERSION");
static mut RUNNING: bool = true;

lazy_static! {
    static ref HANDLER: HashMap<i32, unsafe fn(*mut XEvent)> = map!(
        x11::xlib::ButtonPress      => unimplemented as unsafe fn(*mut XEvent),
        x11::xlib::KeyPress         => keypress as unsafe fn(*mut XEvent),
        x11::xlib::ConfigureRequest => unimplemented as unsafe fn(*mut XEvent),
        x11::xlib::MapRequest       => maprequest as unsafe fn(*mut XEvent),
        x11::xlib::ConfigureNotify  => unimplemented as unsafe fn(*mut XEvent),
        x11::xlib::MappingNotify    => mappingnotify as unsafe fn(*mut XEvent),
        x11::xlib::DestroyNotify    => destroynotify as unsafe fn(*mut XEvent)
    );
    static ref DRWM: Mutex<Drwm> = Mutex::from(Drwm::new());
}

////// EVENT HANDLERS

unsafe fn keypress(ev: *mut XEvent) {
    let rwm = DRWM.try_lock().unwrap();
    let e = XKeyEvent::from(*ev);
    let keysym: x11::xlib::KeySym = XKeycodeToKeysym(rwm.dpy, e.keycode as u8, 0);
    for keyinvoker in config::KEYS.iter() {
        if keysym == keyinvoker.key as u64 && cleanmask!(keyinvoker.modifier, rwm) == cleanmask!(e.state, rwm) {
            drop(rwm);
            (*(keyinvoker.closure))();
            break;
        }
    }
}

unsafe fn maprequest(ev: *mut XEvent) {
    let e = XMapRequestEvent::from(*ev);
    let mut drwm = DRWM.try_lock().unwrap();
    let wa = drwm.get_wa(e.window);
    if wa.is_none() { return; }
    let wa = wa.unwrap();
    if wa.override_redirect == 1 { return; }
    drwm.manage(e.window);
    drwm.testArrange();
}

unsafe fn mappingnotify(ev: *mut XEvent) {
    let mut e = XMappingEvent::from(*ev);
    let mut drwm = DRWM.try_lock().unwrap();
    XRefreshKeyboardMapping(&mut e);
    if e.request == MappingKeyboard { drwm.grab_keys(); }
}

unsafe fn destroynotify(ev: *mut XEvent) {
    let e = XDestroyWindowEvent::from(*ev);
    let mut drwm = DRWM.try_lock().unwrap();
    drwm.unmanage(e.window);
    drwm.testArrange();
}

unsafe fn unimplemented(ev: *mut XEvent) {
    println!("Received an unimplemented event! {:?}", ev);
}

////// STRUCTS

#[allow(dead_code)] 
struct Drwm {
    pub dpy: *mut Display,
    pub root: Window,
    wm_atoms: Vec<Atom>,
    net_atoms: Vec<Atom>,
    pub windows: Vec<Window>,
    numlockmask: u32,
    utf8string: Atom,
    wmcheckwin: Window
}

impl Drwm {
    fn check_other_wm(&self) {
        unsafe {
            self.select_input(XDefaultRootWindow(self.dpy), SubstructureRedirectMask);
            XSync(self.dpy, False);
            XSetErrorHandler(Some(xerror));
            XSync(self.dpy, False);
        }
    }

    fn gnet(&mut self, a: NetAtom) -> &Atom {
        return self.net_atoms.iter().nth(a as usize).unwrap();
    }

    fn setup(&mut self) {
        self.check_other_wm();
        unsafe  {
            XSetErrorHandler(Some(xerror));
            XSync(self.dpy, False);
        }

        // Cleanup Zombies
        sigchld(0);

        unsafe {
            // Atoms
            self.utf8string = XInternAtom(self.dpy, cstr("UTF_8_STRING"), False);
            self.wm_atoms = vec![
                XInternAtom(self.dpy, cstr("WM_PROTOCOLS"), False),
                XInternAtom(self.dpy, cstr("WM_DELETE_WINDOW"), False),
                XInternAtom(self.dpy, cstr("WM_STATE"), False),
                XInternAtom(self.dpy, cstr("WM_TAKE_FOCUS"), False)
            ];
            self.net_atoms = vec![
                XInternAtom(self.dpy, cstr("_NET_ACTIVE_WINDOW"), False),
                XInternAtom(self.dpy, cstr("_NET_SUPPORTED"), False),
                XInternAtom(self.dpy, cstr("_NET_WM_NAME"), False),
                XInternAtom(self.dpy, cstr("_NET_WM_STATE"), False),
                XInternAtom(self.dpy, cstr("_NET_SUPPORTING_WM_CHECK"), False),
                XInternAtom(self.dpy, cstr("_NET_WM_STATE_FULLSCREEN"), False),
                XInternAtom(self.dpy, cstr("_NET_WM_WINDOW_TYPE"), False),
                XInternAtom(self.dpy, cstr("_NET_WM_WINDOW_TYPE_DIALOG"), False),
                XInternAtom(self.dpy, cstr("_NET_CLIENT_LIST"), False)
            ];

            // NetWMCheck support
            let supported_ptr: *const Atom = self.gnet(NetAtom::NetSupported);
            XChangeProperty(self.dpy, self.wmcheckwin, *self.gnet(NetAtom::NetWMCheck), XA_WINDOW, 32, 
                PropModeReplace, (&mut self.wmcheckwin as *const Window) as *const u8 , 1);
            XChangeProperty(self.dpy, self.wmcheckwin, *self.gnet(NetAtom::NetWMName), self.utf8string, 8, 
                PropModeReplace, ccstr("rwm"), 3);
            XChangeProperty(self.dpy, self.root, *self.gnet(NetAtom::NetWMCheck), XA_WINDOW, 32, 
                PropModeReplace, (&mut self.wmcheckwin as *const Window) as *const u8, 1);
    
            // EWMH support
            XChangeProperty(self.dpy, self.root, *self.gnet(NetAtom::NetSupported), XA_ATOM, 32, 
            PropModeReplace, supported_ptr as *const u8, NetAtom::NetLast as i32);
            XDeleteProperty(self.dpy, self.root, *self.gnet(NetAtom::NetClientList));
        }


        unsafe {
            self.grab_keys();
        }
    }

    pub fn select_input(&self, w: Window, m: i64) {
        unsafe { XSelectInput(self.dpy, w, m);}
    }

    pub fn get_wa(&self, w: Window) -> Option<XWindowAttributes> {
        let mut wa: XWindowAttributes = unsafe { std::mem::zeroed() };
        unsafe {
            if XGetWindowAttributes(self.dpy, w, &mut wa) == 0 {
                return None;
            }
            return Some(wa);
        }
    }

    pub fn unmanage(&mut self, w: Window) {
        self.windows.retain(|x| x != &w);
    }

    pub fn manage(&mut self, w: Window) {
        self.select_input_new_window(w);
        self.windows.push(w);
        unsafe { XMapWindow(self.dpy, w); }
    }

    pub fn select_input_new_window(&self, w: Window) {
        let mask = EnterWindowMask|FocusChangeMask|PropertyChangeMask|StructureNotifyMask;
        self.select_input(w, mask);
    }

    unsafe fn update_numlock_mask(&mut self) {
        self.numlockmask = 0;
        let modmap = XGetModifierMapping(self.dpy);
    
        for i in 0..8 {
            for j in 0..(*modmap).max_keypermod {
                if (*modmap).modifiermap.offset((i * (*modmap).max_keypermod + j) as isize) 
                    == &mut XKeysymToKeycode(self.dpy, XK_Num_Lock as u64) {
                        self.numlockmask = 1 << i;
                }
            }
        }
    
        XFreeModifiermap(modmap);
    }
    
    unsafe fn grab_keys(&mut self) {
        self.update_numlock_mask();
        let modifiers: [u32; 4] = [0, LockMask, self.numlockmask, self.numlockmask|LockMask];
        XUngrabKey(self.dpy, AnyKey, AnyModifier, self.root);
        for i in 0..config::KEYS.len() {
            let code = XKeysymToKeycode(self.dpy, config::KEYS[i].key as u64) as i32;
            if code != 0 {
                for j in 0..modifiers.len() {                
                    XGrabKey(self.dpy, code, config::KEYS[i].modifier | modifiers[j],
                        self.root, True, GrabModeAsync, GrabModeAsync);
                }
            }
        }
        
    }

    pub fn get_monitors(&self) -> Vec<Screen> {
        let mut mons = Vec::new();
        let len = unsafe { XScreenCount(self.dpy) };
        for i in 0..len {
            unsafe { mons.push( *XScreenOfDisplay(self.dpy, i) ); }
        }
        return mons;
    }

    pub fn get_all_roots(&self) -> Vec<Window> {
        return self.get_monitors()
                    .into_iter()
                    .map(|mut mon| unsafe {XRootWindowOfScreen(&mut mon)})
                    .collect();
    }

    pub fn get_all_windows(&mut self, root: Window) {

    }

    fn subdivide_area(&self, area: Area, windows : i32) -> Vec<Area> {
        let mut res = Vec::with_capacity(windows as usize);
        let window_w = area.w;
        let window_h = area.h / windows;
        for i in 0..windows {
            println!("{}", i * area.y);
            res.push(Area {
                x: area.x,
                y: area.y + (i * window_h),
                w: window_w,
                h: window_h,
            });
        }
        return res;
    }

    fn place_window_in_area(&mut self, w: &Window, a: &Area) {
        unsafe { XMoveResizeWindow(self.dpy, w.clone(), a.x, a.y, a.w as u32, a.h as u32); }
    }

    pub fn testArrange(&mut self) {
        let bar_height = 16;
        let areas_len = self.windows.len() as i32;
        if areas_len == 0 { return; }
        let mon: Screen = self.get_monitors().iter().nth(0).unwrap().clone();
        let mut areas: Vec<Area> = vec![];
        let mut areac = 0;
        if areas_len == 1 {
            unsafe { XMoveResizeWindow(self.dpy, *self.windows.iter().nth(0).unwrap(), 0, bar_height, mon.width as u32, (mon.height - bar_height) as u32); }
            unsafe { XMapWindow(self.dpy, *self.windows.iter().nth(0).unwrap()); }
        } else {
            let master_area =  Area { x: 0, y: bar_height, w: (config::MFACT * (mon.width as f32)) as i32, h: mon.height - bar_height };
            let subdiv_areas = self.subdivide_area(Area {x: master_area.w, y: bar_height, w: mon.width - master_area.w, h: mon.height - bar_height}, areas_len - 1);
            let mut master_set = false;
            areac = 0;
            self.windows.iter().for_each(|w: &Window|{
                if master_set {
                    let a = subdiv_areas.iter().nth(areac).unwrap();
                    unsafe { XMoveResizeWindow(self.dpy, w.clone(), a.x, a.y + 1, (a.w - 2) as u32, (a.h - 2) as u32); }
                    areac += 1;
                } else {
                    unsafe { XMoveResizeWindow(self.dpy, w.clone(), master_area.x + 1, master_area.y + 1, (master_area.w - 2) as u32, (master_area.h - 2) as u32); }
                    master_set = true;
                }
                unsafe { XMapWindow(self.dpy, *w); }
            });
        }

    }

    pub fn new() -> Self {
        let dpy_ptr = unsafe { XOpenDisplay(null()) };
        let root = unsafe { XDefaultRootWindow(dpy_ptr) };
        let utf8: Atom = unsafe {zeroed()};
        let checkwin = unsafe { XCreateSimpleWindow(dpy_ptr, root, 0, 0, 1, 1, 0, 0, 0) };
        let mut drwm = Self {
            dpy: dpy_ptr,
            root,
            wm_atoms: vec![],
            net_atoms: vec![],
            windows: vec![],
            numlockmask: 0,
            utf8string: utf8,
            wmcheckwin: checkwin
        };
        drwm.setup();
        return drwm;
    }
}

unsafe impl Send for Drwm {}
unsafe impl Sync for Drwm {}

unsafe extern "C" fn xerror(_dpy: *mut Display, ee: *mut XErrorEvent) -> i32 {
    let ee: XErrorEvent = *ee;
    if (ee.error_code == BadWindow)
    || (ee.request_code == 42 && ee.error_code == BadDrawable)
    || (ee.request_code == 74 && ee.error_code == BadDrawable)
    || (ee.request_code == 70 && ee.error_code == BadDrawable)
    || (ee.request_code == 66 && ee.error_code == BadDrawable)
    || (ee.request_code == 12 && ee.error_code == BadMatch)
    || (ee.request_code == 28 && ee.error_code == BadAccess)
    || (ee.request_code == 33 && ee.error_code == BadAccess)
    || (ee.request_code == 62 && ee.error_code == BadDrawable) {
        return 0;
    }

    return 1;
}

extern "C" fn sigchld(_: i32) {
    unsafe {
        let handler = SigHandler::Handler(sigchld);
        if signal(Signal::SIGCHLD, handler).is_err() {
            utils::die("can't install SIGCHLD handler");
        }
    }
}

fn stop() {
    unsafe { RUNNING = false }
}

unsafe fn spawn(args: &[&str]) {
    let DPY: *mut Display = DRWM.try_lock().unwrap().dpy;
    if fork() == 0 {
        if !DPY.is_null() {
            nix::libc::close(XConnectionNumber(DPY));
        }
        setsid();
        let program: *const c_char = CString::new(args[0]).unwrap().into_raw();
        let cstr_argv: Vec<_> = args.iter()
            .map(|arg| CString::new(*arg).unwrap())
            .collect();
        let mut p_argv: Vec<_> = cstr_argv.iter() 
                .map(|arg| arg.as_ptr())
                .collect();
        p_argv.push(std::ptr::null());
        let argss: *const *const c_char = p_argv.as_ptr();
        println!("{:?}", args.split_first());
        execvp(program, argss);
        perror(CString::new(format!("{} failed", args[0])).unwrap().as_c_str().as_ptr());
        nix::libc::exit(EXIT_SUCCESS);
    }
}

unsafe fn run() {
    let mut ev = MaybeUninit::uninit();
    let dpy = DRWM.try_lock().unwrap().dpy;

    XSync(dpy, False);

    while RUNNING && XNextEvent(dpy, ev.as_mut_ptr()) == 0 {
        let mut ev = ev.assume_init(); 
        //println!("EVENT! {:?}", ev.type_);
        if HANDLER.contains_key(&ev.type_) {
            HANDLER[&ev.type_](&mut ev);
        }
    }
}

fn main() {
    println!("Starting drwm..."); // MFD
    let args : Vec<String> = std::env::args().collect();
    let argc = args.len();
    if argc == 2 && args.into_iter().nth(1).unwrap().eq("-v") {
        utils::die(format!("drwm {}", VERSION).as_str());
    } else if argc != 1 {
        utils::die("usage: drwm [-v]");
    }

    unsafe {
        run();
        XCloseDisplay(DRWM.try_lock().unwrap().dpy);
    }
}
