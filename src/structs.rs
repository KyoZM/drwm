use nix::libc::c_uint;


#[allow(bare_trait_objects)]
pub struct KeyInvoker {
    pub modifier: c_uint,
    pub key: c_uint,
    pub closure: *const Fn() -> (),
}

pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8
}

//TODO
impl Color {
    pub const fn new(hex: &str) -> Self {
        return Self {
            r: 1_u8,
            g: 1_u8,
            b: 1_u8,
        }
    }
}


#[derive(Debug)]
pub struct Area {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}