use nix::libc::c_uint;
use structs::KeyInvoker;
use x11::xlib::{Mod2Mask as SuperMask, ControlMask, Mod1Mask as AltMask, ShiftMask};
use crate::*;
use x11::keysym::*;
use std::time::Instant;
use std::process::Command;

const NORM_BORDER_COLOR: structs::Color =   color!("#444444");
const NORM_BG_COLOR: structs::Color =       color!("#222222");
const NORM_FG_COLOR: structs::Color =       color!("#bbbbbb");
const SEL_FG_COLOR: structs::Color =        color!("#eeeeee");
const SEL_BORDER_COLOR: structs::Color =    color!("#770000");
const SEL_BG_COLOR: structs::Color =        color!("#005577");

pub static MFACT: f32 = 0.5;
pub static NMASTER: i32 = 1;

#[allow(unused_unsafe)]
pub const KEYS: &'static[KeyInvoker] = &[
    kbind!( AltMask,                XK_t,       {
        let time = Command::new("date").output().expect("Couldn't run 'date'");
        println!("{:?}", String::from_utf8(time.stdout));
    }),
    
    kbind!( AltMask | ShiftMask,    XK_Return,  { stop() }),
    kbind!( AltMask | ShiftMask,    XK_d,       { spawn(&["st"]) }),
];