use std::ffi::CString;

use nix::libc::c_char;
#[allow(unused_imports)]
use x11::xlib::{Atom, ControlMask, Mod1Mask, Mod2Mask, Mod3Mask, Mod4Mask, Mod5Mask, ShiftMask};

// Config

#[macro_export]
macro_rules! color {
    ($hex:expr) => {
        structs::Color::new($hex);
    };
}

#[macro_export]
macro_rules! kbind {
    ($mod:expr, $key:expr, $closure:block) => {
        KeyInvoker {
            modifier: $mod,
            key: $key,
            closure: & || unsafe{ $closure },
        }
    };
}

#[macro_export]
macro_rules! arr {
    ($id: ident $name: ident: [$ty: ty; _] = $value: expr) => {
        $id $name: [$ty; $value.len()] = $value;
    }
}

#[macro_export]
macro_rules! cleanmask {
    ($mask:expr, $m:expr) => {
        ($mask & !($m.numlockmask|LockMask) & (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask))
    };
}

// Main

pub fn die(msg: &str) {
    println!("{}", msg);
    std::process::exit(1);
}

pub fn cstr(s: &str) -> *mut c_char {
    return CString::new(s).unwrap().into_raw();
}

pub unsafe fn ccstr(s: &str) -> *const u8 { // same but different
    return CString::new(s).unwrap().into_raw() as *const u8;
}